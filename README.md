# Usefull manuals

## General knowledge

* [General VNC and SSH guide](VNC_and_SSH_Manual.md) --- how to run your VNC and forward ports to use it locally on your machine

## Feb21 experiment

* [Offline DST manual](Feb21DstOffline.md) --- how to preapre and run DST and offline analysis for (primarly) STS detector.