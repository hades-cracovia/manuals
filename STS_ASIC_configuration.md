# STS configuration

## General remarks:

- perform all operations on `hades30`
- STS operator VNC (hades33:3)
- STS operator HTTP VNC: https://coop.gsi.de/hades-sts (not yet running)
- before start of any operation, load profile for STS:
```bash
. /home/hadaq/trbsoft/hadesdaq/sts/settings/profile.sh
```
The profile will export two variables:
- `sts1_trbids`
- `sts2_trbids`

which hold all thrbids relevant for STS1 and STS2, and which can be used as arguments for python commands below (bash scripts use them internally), in following way:
```bash
some_tool.py "${sts1_trbids}"
```

## Helpfull commands

### Load default settings for STS1 or STS2
```bash
bash /home/hadaq/trbsoft/hadesdaq/sts/settings/set_config_sts1.sh
bash /home/hadaq/trbsoft/hadesdaq/sts/settings/set_config_sts2.sh
```

### Adjust threshold for given trbid
```bash
asic_threshold.py -Vth val trbid
```

where:
- `val` - new threshold value
- `trbid` - one or more trb id to set, the `trbid` can have one of forms:
 - `addr` -- is a hex addres, like 0x6400
 - `addr:cable` -- cable is cable number: 1,2,3, can by any combination separated by comma: 1 or 1,2 or 1,3 or 2,3 etc
 - `addr:cable:asic` -- asic is asic number: 1 or 2

Examples:
- `0x6400:1,2` -- loads all asics on cable 1 and 2 of 0x6400
- `0x6400::1` -- loads asic 1 on all cables 1-3 of 0x6400
- `0x6400` -- loads all asics for given trb id

This addressing works the same for all python tools from pasttrectool package mentioned below.

### Set threshold to all STS1 or STS2 asics:
```bash
/home/hadaq/trbsoft/hadesdaq/sts/settings/set_thresh_sts1.sh val
/home/hadaq/trbsoft/hadesdaq/sts/settings/set_thresh_sts2.sh val
```
where `val` is new threshold value.

> WARINING: There is an unknown issue with setting threshold that for the first call only odd channels groups (32 channels) are set and second call is required to configure all thresholds.

### Set any register
```bash
asic_set_reg.py [-h] [-v {0,1,2,3}] [-Vth THRESHOLD] trbids reg val
```

### Read asic configuration:
```bash
asic_read.py [-h] trbids [trbids ...]
```

### Reset asic:
```bash
asic_reset.py [-h] trbids [trbids ...]
```
or to reset all STS1 or STS2:
```bash
/home/hadaq/trbsoft/hadesdaq/sts/settings/reset_asics_sts1.sh
/home/hadaq/trbsoft/hadesdaq/sts/settings/reset_asics_sts2.sh
```
### Communication test
```bash
communication_test.py [-h] [-q] trbids [trbids ...]
```
where:
- `-q` -- quick, tests only one register, by default each register is tested by writing multiple values (takes longer time)

As communication tests overwrites register settings, it is required to reconfigure the ASICs afterwards.