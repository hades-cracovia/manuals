# STS@HADES - Online monitoring using Go4

The online monitoring of STS detectors with GO4 is availabe on VNC displays:
- `lxhadeb06:20` for STS1
- `lxhadeb06:21` for STS2

Please read [VNC and SSH manual](VNC_and_SSH_Manual.md) for instructions.

If the monitoring is not operating or you want to run your own instance, you can use methods described below.

## Event builder requirements

The online monitor captures data from the HADES event build so *be sure that EB is running* and storing data to a file or is set to `NO_FILE` mode. Data are taken from eventbuilded `lxhadeb08:8101`.

## Launching Online Monitoring

1. Open the VNC display from `lxhadeb06:17`

2. Enter the following commands to start go4 GUI interface:
```bash
cd  /home/fwdet-sts/STSGo4
. /home/hadaq/local/bin/trb3login   
. /home/hadaq/local/bin/go4login
go4
```

3. Click on the “Launch analysis” button (encircled green button).

4. Set the Dir to `/home/fwdet-sts/STSGo4` and click on the green tick, (this is where the essential scripts `first.C` and `second.C` are located. Ccan be adjusted to a location of your custom scripts).

5. Set the analysis configuration:
 - Event source: `MBS Stream Server`
 - Name: `lxhadeb08:8101`

 and click on `Submit+Start`

6. Click on `Start monitoring of visible objects` (green button on top left)

7. In the object browser in the left, navigate to `Analysis > Histograms > .. ` and choose the histograms to be visualized.

> *TIPS:* The generated root file will be automatically saved on termination of go4 by the name of `Go4AutoSave.root`, rename the file before launching go4 the next time if you want to save it.

### Using it for offline data

To run analysis on a pre-collected hld file, in the `Analysis configuration` window select the:
- Event source: `User source`
- Name: choose the file you want to analyse (files are usually stored located in path: /store/08/01/data/)

## Launch in batch mode

- For MBSStream data
```bash
go4analysis -stream lxhadeb08:8101
```
- For user data:
```bash
go4analysis -user /path/to/file -num 1000
```
