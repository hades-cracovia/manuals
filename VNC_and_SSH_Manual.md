# VNC and SSH General Guild for hadeb06 and other machines

## General remarks

* All VNC conenctions and SSH tunnels must be initialzied from your local PC, not GSI. Opening the tunnel on the GSI (`lx-pool`) machine will forward all the ports there and there will not be access to them from your local PC.

## Custom VNC display
To create new vnc display use follwoing command:
```
vncserver :XYZ -AlwaysShared
```
where `XYZ` is display number. The display is related to the port number as `5900+XYZ`, so the proper prot must be free (you can check it with `lsof -i:PORT` where `PORT` is tested port number).

## SSH tunnels

In order to work on remote vnc from local (office) computer, you must forward the port. Recommended way is to use ssh tunnels.

### SSH config

Create or edit your `~/.ssh/config` and add following entry:
```
Host                gsi
Hostname            lx-pool.gsi.de
User                <your gsi username here>
TCPKeepAlive        yes

Host                hadeb06
User                <lxhadeb06 account name here>
Hostname            lxhadeb06.gsi.de
ProxyJump           gsi
LocalForward        5904 localhost:5904
```
This config forward display `4` of `lxhadeb06` (hades online monitoring) to your localhost.

After the config is ready, instantiate new connection with:
```bash
ssh hadeb06
```
which will automaticaly create tunnel from remote port to local port.

### Custom port forwards
You can forward any port you want by adding next entries in the form of
```
LocalForward        LOCAL_PORT localhost:REMOTE_PORT
```
where `REMOTE_PORT` must match the port as given above (`5900+DISPLAY`) and `LOCAL_PORT` can be anything above 1023 (system reserved port number).

After adding new forward port definitions, you must reconnect your ssh session or open new connection.

### Using local ports

With ports forwarded, you can see VNC displays with (vnc automaticly adds 5900 to the display number):
```bash
vncviewer localhost:DISPLAY
```
or
```bash
vncviewer localhost:LOCAL_PORT
```
`vncviewer` accepts both.
If you forward remote port from VNC to local port which does not follow the pattern `5900+DISPLAY`, then you must use the second way to make it working.