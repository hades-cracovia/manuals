# Online DST

Is up to you where you do you analysis, but the best is to use dedicated account on `lxhadeb06.gsi.de`. Ask for account name and password person in charge of your detector software.

The `lxhadeb06` machines provides all software and storage required for performing analysis. It is recommend to create in the home directory of your account an directory which identifies you and work only there to do not flood home directory with many files. Also, for the convenience of work, use vnc display. Ask person responsible for your detector software to create a vnc for you, or do it by yourself.
Please follow [VNC and SSH manual](VNC_and_SSH_Manual.md) for details.

## Feb21 online server

The vnc providing the online view is `lxhadeb06.gsi.de:4`. From outside GSI please use SSH tunneling to forward port to localhost.

## Developemnt
Online Monitoring code is available from:
* https://git.gsi.de/hades-cracovia/software/hades-online

Online monitoring requires hydra and ROOT-6 which is loaded with following profile:
```bash
. /home/hadaq/local/bin/hydralogin
```

# Custom DST

## Prerequisites

For any work with DST and offline code you need to load the proper profile which setup ROOT-5 and hydra for you:
```bash
. /home/hadaq/local/bin/5hydralogin
```

## Source code
To run custom dst one needs a dst code which is to be found here:
* https://git.gsi.de/hades-cracovia/software/feb21_dst_template

Follow documentation for details on installation and usage. Please also remember to check out for new parameters version before running larger DST work. You can update from repository and update the parameters with following commands:
```bash
git pull
cd feb21_params
./gen_params.sh
```

# File storage

The files from event bulders arestored in `/store/08/01/data` on `lxhadeb06`. Use these fiels as input for your dst analysis, e.g.

```bash
./analysisDST /store/08/01/data/fs2103416194801.hld odir 1000000
```
`odir` is the output direcotry and must exist.

Typical hld file contains around 300k events so giving limit at 1000000 should analyse the whole file always, but always be sure of it.

# Offline analysis

## STS 1 & 2

The source code for STS1 and STS2 is available here:
* https://git.gsi.de/hades-cracovia/software/feb21_offline

Follow repo documentation for installation and usage guides. If you want to contribute with new code, like new histograms, create your branch, commit changes and create Merge Request.

> **Free tips:** I recommend to download the repository into dst directory. Then after compiling and installing, the `drawhists` executable will show up on the same directory level like `analysisDST`.

Example of using with the file unpacked above:
```bash
./drawhists odir/fs2103416194801* -o some_name.root -e 1000000
```
The `*` means take the full name of file. You can also use patterns to input more tha one file, like:
```bash
./drawhists odir/fs21034* -o some_name.root -e 1000000
```
which will draw histograms from all unpacked files from 3rd of February 2021. If you are puting many files, then you might want to increase the events parameter `-e`. But be carefull, is time consuming.

> **Free tips:** Run offline on rather small samples for cross-check. When ready, run it over the night and go sleep.

For meaning of histograms, check documentation of the repository. If histogram is not documented, ask author of it to update the documentation.